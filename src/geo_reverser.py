import requests
import json

from geopy.exc import GeocoderUnavailable

from flask import Flask, request
from flask_caching import Cache

from geopy.geocoders import Nominatim

from configuration import config
from geo_utilities.verbose import VerboseAddress
from geo_utilities.landmark_distance import LandmarkDistance


app = Flask('application')
app.config.from_mapping(config)
cache = Cache(app)
LANDMARK_DISTANCE = LandmarkDistance()


@app.route("/api/v1/reverse")
def reverse():
    latitude = request.args.get("latitude", None)
    longitude = request.args.get("longitude", None)
    if latitude is None or longitude is None:
        return "N.N."
    else:
        longitude = float(longitude)
        latitude = float(latitude)

    return get_address(latitude, longitude)


@cache.memoize()
def get_address(latitude: float, longitude: float) -> str:

    geolocations = Nominatim(
        user_agent="EndTecGPS Geo-reverser",
        domain=config["NOMINATIM_URL"],
        scheme='http'
    )

    try:
        nmn_response = geolocations.reverse(
            "{}, {}".format(latitude, longitude),
            # Zoom 17 will calculate all the way down to street level, where 18 can sometimes
            # return buildings.
            zoom=17,
            language="es"
        )
        street = nmn_response.raw
        # If the returned type is not a way (node or relation), return early, as there is no
        # street data to use.
        if street["osm_type"] != "way":
            if nmn_response.address != "":
                return nmn_response.address
            raise AttributeError
    except (AttributeError, GeocoderUnavailable, KeyError):
        return "N.N."

    osm_id = street["osm_id"]

    content = '[out:json];' \
              '(way(id:{});)->.y;' \
              'node(w)->.b;' \
              '(way(around:0)[highway~"."][highway!~"path|track|cycleway|footway|secondary_link|service|primary_link"];)->.z;' \
              '(.z; - .y;)->.x;' \
              '(.x;.x>;)->.k;' \
              '(node.b.k;)->.c;' \
              '.y out;' \
              '.x out geom qt;' \
              '.c out qt;' \
        .format(
            osm_id
        )

    response = requests.post(
        url=config["OVERPASS_URL"],
        data=content
    )

    response_elements = json.loads(response.text)["elements"]

    verbose_address = VerboseAddress(
        origin=(latitude, longitude),
        nmn_response=street,
        osm_elements=response_elements,
        landmark_distance=LANDMARK_DISTANCE
    ).verbose_address(
    )

    return verbose_address
