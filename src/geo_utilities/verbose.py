from .distance import shortest_point_to_segment
from .landmark_distance import LandmarkDistance
from configuration import app_config


class VerboseAddress:

    def __init__(self, origin: tuple[float, float], nmn_response: dict, osm_elements: dict,
                 landmark_distance: LandmarkDistance):

        self.address = nmn_response["address"]
        self.osm_id = nmn_response["osm_id"]
        self.osm_response = osm_elements
        self.landmark_distance = landmark_distance
        self.latitude, self.longitude = origin

    def verbose_address(self) -> str:
        if self.is_countryside():
            return self.verbose_countryside_geo_address()

        return self.verbose_city_geo_address()

    def verbose_city_geo_address(self) -> str:
        """
        Verbose a geo-decoded city address returning the road, neighbourhood,
        suburb, city and country, if available. When not available, skip them.
        """

        address_block = "{}, "
        result_address = ""
        streets = self.get_before_and_next_intersections()
        # Remove duplicate streets
        streets = list(set(streets))

        if "road" in self.address:
            try:
                streets.remove(self.address["road"])
            except ValueError:
                pass

            if len(streets) == 0:
                result_address += address_block.format(
                    self.address["road"]
                )
            elif len(streets) == 1:
                result_address += "{} con {}, ".format(
                    self.address["road"],
                    streets[0]
                )
            else:
                result_address += "{} entre {} y {}, ".format(
                    self.address["road"],
                    streets[0],
                    streets[1]
                )

        location_keys = app_config["CITY_KEYS"]

        used_values = []
        for key in location_keys:
            try:
                value = self.address[key]
                if value not in used_values:
                    result_address += address_block.format(self.address[key])
                    used_values.append(value)
            except KeyError:
                continue

        return result_address[:-2] if result_address.endswith(", ") else result_address

    def verbose_countryside_geo_address(self) -> str:
        """
        Verbose a geo-decoded countryside city address returning the road,
        hamlet, county, state and country, when available, plus the distance
        to the closest landmark (city, or town).
        """

        address_block = "{}, "
        result_address = ""
        location_keys = app_config["COUNTRYSIDE_KEYS"]
        nearby = self.landmark_distance.closest(self.latitude, self.longitude)

        # If LandmarkDistance is not updating
        if nearby != (0, ""):
            if "road" in self.address:
                result_address += address_block.format(
                    "{} a {} de {}".format(
                        self.address["road"],
                        self.verbose_distance(nearby[0]),
                        nearby[1]
                    )
                )
            else:
                result_address += address_block.format(
                    "A {} de {}".format(
                        self.verbose_distance(nearby[0]),
                        nearby[1]
                    )
                )
        else:
            if "road" in self.address:
                result_address += address_block.format(
                    self.address["road"]
                )

        if result_address != "" and not result_address.startswith(app_config["ROAD_NAMES"]):
            result_address = "Vía " + result_address

        used_values = []
        for key in location_keys:
            try:
                value = self.address[key]
                if value not in used_values:
                    result_address += address_block.format(value)
                    used_values.append(value)

            except KeyError:
                continue

        return result_address[:-2] if result_address.endswith(", ") else result_address

    def get_before_and_next_intersections(self) -> list[str, ...]:
        ways_list = []
        nodes_list = []
        main_way = None

        for element in self.osm_response:
            if element["type"] == "way":
                if element["id"] == self.osm_id:
                    main_way = element
                else:
                    ways_list.append(element)

            elif element["type"] == "node":
                nodes_list.append(element)

        intersection_segments = []
        last_node = None

        for main_node in main_way["nodes"]:
            for way_node in nodes_list:
                if way_node["id"] == main_node:
                    if last_node is None:
                        last_node = way_node
                        continue
                    intersection_segments.append((last_node, way_node))
                    last_node = way_node

        streets = []
        if len(intersection_segments) == 0:
            return streets

        closest_segment = shortest_point_to_segment((self.latitude, self.longitude), intersection_segments)

        for way in ways_list:
            for node in way["nodes"]:
                if node == closest_segment[0]["id"] or node == closest_segment[1]["id"]:
                    try:
                        self.__raise_at_collision(way, main_way)
                        streets.append(way["tags"]["name"])

                    except KeyError:
                        continue
        return streets

    def is_countryside(self) -> bool:
        """
        Checks if whether the geodecoded address is located in the
        countryside.
        """

        if "road" in self.address and self.address["road"].startswith(app_config["CITY_ROADS"]):
            return False
        elif "town" in self.address or "city" not in self.address or \
                ("road" in self.address and self.address["road"].isnumeric()):
            return True

        return False

    @staticmethod
    def __raise_at_collision(way1: dict, way2: dict) -> None:
        """
        Takes two ways as parameters. If tags.ref values are the same for both
        parameters, raise KeyError.
        """

        try:
            if way1["tags"]["name"] == way2["tags"]["name"] or way1["tags"]["ref"] == way2["tags"]["ref"]:
                raise ValueError
        except KeyError:
            pass
        except ValueError:
            raise KeyError

    @staticmethod
    def verbose_distance(distance: float) -> str:
        """
        Will verbose a distance in kilometers adding the distance unit. If it's
        bellow a kilometer, convert it to meters.
        """

        if distance < 1.0:
            return "{} m".format(int(distance * 1000))
        else:
            return "{:.1f} km".format(distance)
