from operator import itemgetter

from .optimized_math.cartesian import point_to_segment_distance as c_point_to_segment_distance


def shortest_point_to_segment(point: tuple[float, float], segments: list[tuple[dict, dict]]) -> tuple[dict, dict]:
    """
    Takes two arguments, point being the starting point to measure from,
    and segments being a list, each made out of two nodes forming a line
    segment. Will return the closest segment to the point.

    Uses WGS84 coordinates.
    """

    results = []
    for line_segment in segments:
        p_to_s_distance = c_point_to_segment_distance(
            point[0],
            point[1],
            line_segment[0]["lat"],
            line_segment[0]["lon"],
            line_segment[1]["lat"],
            line_segment[1]["lon"]
        )
        results.append((p_to_s_distance, line_segment))

    # Sort from smallest to biggest, get first item (smallest) and return its line_segment
    return sorted(results, key=itemgetter(0))[0][1]
