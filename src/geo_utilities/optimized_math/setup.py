from setuptools import setup
from distutils.extension import Extension
from Cython.Build import cythonize

extensions = [
    Extension("haversine", ["haversine.pyx"]),
    Extension("cartesian", ["cartesian.pyx"])
]

setup(
    ext_modules=cythonize(extensions)
)
