# Refer to /docs/Math.md for more information.
from libc.math cimport cos, asin, sqrt


cpdef float haversine_distance(float lat1, float lon1, float lat2, float lon2):
    """
    Calculates distances between two given WGS84 points accurately, but it's slower.
    Use for long distance point to point calculations. Returns distance in KM.
    """

    # Baked haversine factor
    cdef float p = 0.0174532925
    cdef float hav = 0.5 - cos((lat2-lat1)*p)/2 + cos(lat1*p)*cos(lat2*p) * (1-cos((lon2-lon1)*p)) / 2
    cdef float result = 12742 * asin(sqrt(hav))
    return result
