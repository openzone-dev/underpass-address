# Refer to /docs/Math.md for more information.
from libc.math cimport cos, sin, sqrt


earth_radius = 6378.1370

cdef float radians(float degrees):
    """
    Converts degrees to radians, using a baked
    pi/180 value.
    """
    cdef float half_pi = 0.017453292
    return half_pi * degrees

# region WGS84 to Cartesian
"""
Converts WGS84 units onto its 3D space Cartesian coordinate
equivalent.
"""

cdef float x_cartesian(float latitude, float longitude):
    return earth_radius * cos(radians(latitude)) * cos(radians(longitude))

cdef float y_cartesian(float latitude, float longitude):
    return earth_radius * cos(radians(latitude)) * sin(radians(longitude))

cdef float z_cartesian(float latitude):
    return earth_radius * sin(radians(latitude))

# endregion

cdef float pythagorean_length(float x, float y, float z):
    """
    Calculates the length of the given coordinates, as a triangle.
    """
    return sqrt(x * x + y * y + z * z)

cpdef float point_to_segment_distance(float o_latitude, float o_longitude,
                                      float s1_latitude, float s1_longitude,
                                      float s2_latitude, float s2_longitude):
    """
    Calculates the projection from a given o point to a line segment formed from
    s1 and s2 points, and returns the shortest distance possible to the line.
    
    The distance returned is in Cartesian system, and therefore is in no metrical
    units. Convert it back by applying an inverse formula.
    
    Doesn't take the earth curvature onto account, as it should be used to measure short 
    distances.
    """

    # Convert WGS84 to Cartesian plane
    cdef float s1_x = x_cartesian(s1_latitude, s1_longitude)
    cdef float s1_y = y_cartesian(s1_latitude, s1_longitude)
    cdef float s1_z = z_cartesian(s1_latitude)

    # Convert cartesian coordinates to vectors
    cdef float line_vec_x = x_cartesian(s2_latitude, s2_longitude) - s1_x
    cdef float line_vec_y = y_cartesian(s2_latitude, s2_longitude) - s1_y
    cdef float line_vec_z = z_cartesian(s2_latitude) - s1_z

    cdef float point_vec_x = x_cartesian(o_latitude, o_longitude) - s1_x
    cdef float point_vec_y = y_cartesian(o_latitude, o_longitude) - s1_y
    cdef float point_vec_z = z_cartesian(o_latitude) - s1_z

    # Scale and Unit both vectors and clamp t (clamp)
    cdef float line_length = pythagorean_length(
        line_vec_x,
        line_vec_y,
        line_vec_z
    )
    cdef float scale_factor = 1.0 / line_length

    cdef float t = (line_vec_x / line_length) * (point_vec_x * scale_factor) +\
                   (line_vec_y / line_length) * (point_vec_y * scale_factor) +\
                   (line_vec_z / line_length) * (point_vec_z * scale_factor)

    if t < 0.0:
        t = 0.0
    elif t > 1.0:
        t = 1.0

    return pythagorean_length(
        point_vec_x - (line_vec_x * t),
        point_vec_y - (line_vec_y * t),
        point_vec_z - (line_vec_z * t)
    )
