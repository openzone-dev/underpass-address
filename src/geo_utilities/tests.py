from math import cos, sin, atan2, degrees, asin, radians
import math
import requests


# TODO WIP V2 version Proof of concept
# def is_point_in_path(x: float, y: float, poly) -> bool:
#     num = len(poly)
#     j = num - 1
#     c = False
#     for i in range(num):
#         if (x == poly[i][0]) and (y == poly[i][1]):
#             # point is a corner
#             return True
#         if (poly[i][1] > y) != (poly[j][1] > y):
#             slope = (x - poly[i][0]) * (poly[j][1] - poly[i][1]) - (poly[j][0] - poly[i][0]) * (y - poly[i][1])
#             if slope == 0:
#                 # point is on boundary
#                 return True
#             if (slope < 0) != (poly[j][1] < poly[i][1]):
#                 c = not c
#         j = i
#     return c
#
#
# if __name__ == "__main__":
#     # last car position
#     lat1 = math.radians(4.538975870053764)
#     long1 = -74.24628693714429
#
#     # current car position
#     lat2 = math.radians(4.537968694536177)
#     long2 = -74.24549776966714
#
#     dLon = (long2 - long1)
#     x = math.cos(lat2) * math.sin(math.radians(dLon))
#     y = math.cos(lat1) * math.sin(lat2) - math.sin(lat1) * math.cos(lat2) * math.cos(math.radians(dLon))
#     bearing = math.atan2(x, y)
#
#     # TODO try without this if it fails
#     #bearing = math.degrees(bearing)
#     print(degrees(bearing))
#
#     # Triangle edges
#     long2 = math.radians(long2)
#     earth_radius = 6378.1370
#     distance = 20
#     ad = distance / earth_radius
#
#     tr1_lat = asin(sin(lat2) * cos(ad) + cos(lat2) * sin(ad) * cos(bearing-radians(30)))
#     tr1_lon = long2 + atan2((sin(bearing) * sin(ad) * cos(lat2)), (cos(ad) - sin(lat2) * sin(tr1_lat)))
#
#     tr2_lat = asin(sin(lat2) * cos(ad) + cos(lat2) * sin(ad) * cos(bearing+radians(30)))
#     tr2_lon = long2 + atan2((sin(bearing) * sin(ad) * cos(lat2)), (cos(ad) - sin(lat2) * sin(tr2_lat)))
#
#     print("{}, {}".format(degrees(tr1_lat), degrees(tr1_lon)))
#     print("{}, {}".format(degrees(tr2_lat), degrees(tr2_lon)))
#
#     a = is_point_in_path(
#         4.547490095714475,
#         -74.25140417854097,
#         [(4.537968694536177, -74.24549776966714),
#          (degrees(tr1_lat), degrees(tr1_lon)),
#          (degrees(tr2_lat), degrees(tr2_lon))]
#     )
#
#     print(a)


# Test coordinates
if __name__ == "__main__":
    coordinate_list = (
        (4.648184666666666, -74.12786633333333),
        (5.026578, -74.003347),
        (4.6606145, -74.11663333333334),
        (4.808332, -75.691432),
        (5.036243, -73.99547433333333),
        (4.616173, -74.175447),
        (4.630055, -74.131097),
        (6.24614, -75.58292),
        (6.4401, -75.3383),
        (4.69847, -74.07912)
    )

    for coordinate in coordinate_list:
        url = "http://127.0.0.1:5000/api/v1/reverse?latitude={}&longitude={}".format(coordinate[0], coordinate[1])
        print(
            requests.get(url).text
        )
