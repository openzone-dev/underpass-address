import requests
import json
import pickle
import os

from operator import itemgetter
from configuration import config
from .optimized_math.haversine import haversine_distance


class LandmarkDistance:
    """
    Handles Landmark Distance calculations using a file generated at
    startup from geographical data. The file will be generated only
    if it doesn't exist, or if it's requested at runtime.

    Thread-safe, and will return empty values when performing a
    file update.
    """

    landmark_path = config["PICKLE_STORAGE_PATH"]+"/landmarks.pkl"

    def __init__(self):
        self.ready = False

        if os.path.exists(self.landmark_path):
            self.landmark_list = self.__load_landmark_list()
        else:
            self.landmark_list = self.__generate()
        self.ready = True

    def closest(self, lat: float, lon: float) -> tuple[float, str]:
        """
        Given the WGS84 coordinates for a geographical point, find
        the closest landmark, and return it, where the distance
        is the tuple's first item, in km, and the second item is
        the landmark name.

        Thread-safe.
        """

        if not self.ready:
            return 0, ""

        distance_list: list[tuple[float, str]] = []
        for landmark in self.landmark_list:
            distance_list.append(
                (haversine_distance(lat, lon, landmark[0], landmark[1]), str(landmark[2]))
            )

        return sorted(distance_list, key=itemgetter(0))[0]

    def regenerate(self) -> None:
        """
        Triggers a landmark file regeneration. If other threads are currently
        using the landmark file, they may (if they're very slow) except gracefully
        and exit.

        Thread-safe in ideal conditions.
        """

        if not self.ready:
            return
        self.ready = False
        self.landmark_list = self.__generate()
        self.ready = True

    def __generate(self) -> list[list[float, float, str]]:
        """
        Generates a landmark file, and returns the contents to
        be loaded into memory as a static variable.

        The file is a pickled list of lists, where each child
        list contains latitude, longitude, (both in WGS84) and name. Ej:

        [[1.00202, -73.21039, "Bogota"], [8.00202, -79.21039, "Barranquilla"], ...]

        The list is not stored in database to avoid overhead, and will always
        be read from memory, that, after being loaded from storage, or generated.
        """

        landmark_list = ["town", "city"]
        response_list: list[dict, ...] = []

        for landmark in landmark_list:
            content = '[out:json];area[name="Colombia"];(node[place="{}"];);out;' \
                .format(landmark)

            response = requests.post(
                url=config["OVERPASS_URL"],
                data=content
            )

            response_list.append(json.loads(response.text))

        fields_list = ["lat", "lon", "name"]

        # Store in list of lists, in the following fashion, and order:
        # [[lat, lon, name], [lat, lon, name], ...]
        result: list[list[float, float, str]] = []

        for response in response_list:
            for node in response["elements"]:
                extracted_data = []
                for field in fields_list:
                    try:
                        if field == "name":
                            extracted_data.append(str(node["tags"][field]))
                        else:
                            extracted_data.append(node[field])
                    except KeyError:
                        break

                if len(extracted_data) != 3:
                    continue

                result.append(extracted_data)

        if os.path.exists(self.landmark_path):
            os.remove(self.landmark_path)

        os.makedirs(config["PICKLE_STORAGE_PATH"], exist_ok=True)

        with open(self.landmark_path, 'wb') as handle:
            pickle.dump(result, handle)

        return result

    def __load_landmark_list(self) -> list[list[float, float, str]]:
        """
        Loads a landmark list from storage onto memory.
        """

        with open(self.landmark_path, 'rb') as handle:
            local_list = pickle.load(handle)
        return local_list
