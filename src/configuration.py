import os


def get_nominatim_url() -> str:
    nominatim_host = os.getenv("NOMINATIM_HOST", "127.0.0.1")
    return "{}:{}".format(nominatim_host, 8080)


def get_overpass_url() -> str:
    overpass_host = os.getenv("OVERPASS_HOST", "127.0.0.1")
    return "http://{}:{}/api/interpreter".format(overpass_host, 80)


config = {
    # Flask
    "DEBUG": bool(os.getenv("DEBUG", False)),

    # region Flask-Caching
    "CACHE_TYPE": "FileSystemCache",
    # Cache timeout in seconds, defaults to an hour
    "CACHE_DEFAULT_TIMEOUT": int(os.getenv("CACHE_TIMEOUT", 3600)),
    # More cache will speed up repetitive queries, but indexing the folder will be slower
    "CACHE_THRESHOLD": int(os.getenv("CACHE_FILE_LIMIT", 5000)),
    "CACHE_DIR": "/var/tmp/geo_cache",
    "CACHE_IGNORE_ERRORS": True,
    # endregion

    # region Database and Storage
    "OVERPASS_URL": get_overpass_url(),
    "NOMINATIM_URL": get_nominatim_url(),
    "PICKLE_STORAGE_PATH": "/var/lib/geo-address"
    # endregion
}

app_config = {
    # General usage road name list, must contain all road names for the country
    # The current usage of this list is to improve countryside address verbosity
    "ROAD_NAMES": ("Variante", "Troncal", "Autopista", "Avenida", "Vía", "Via", "Carrera", "Calle", "Carretera",
                   "Transversal", "Diagonal", "A "),

    # If these are detected in the address, city mode will take over instead of countryside
    "CITY_ROADS": ("Carrera", "Calle", "Transversal", "Diagonal", "Avenida"),

    # Keys to verbose addresses, in left to right, order of importance (Keys from Nominatim)
    "CITY_KEYS": ("neighbourhood", "suburb", "town", "city", "county", "region", "country"),
    "COUNTRYSIDE_KEYS": ("town", "hamlet", "county", "state", "region", "country")
}
