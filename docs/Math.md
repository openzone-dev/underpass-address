# Optimized Math
`geo_utilities.optimized_math`

The optimized math module does geo relevant operations, and strives to be fast at doing so. For that reason, these
modules are written in Cython, and compiled beforehand. They are usually compiled by the Dockerfile at build, or
compiled just before runtime for the development version.

## Calculations (Math, Algebra)

As one of the focus for this microservice is speed, all calculations are done by the microservice using linear algebra,
and math in general. The usage of database bound polygons and queries reliant on database processing
should be avoided. This way, if the microservice has to be scaled due to high traffic, another instance of 
this microservice can be created, connected to the same databases, and used with a load balancer, avoiding the spawning
of more costly databases.

To understand the following formulas and calculations, one must have basic knowledge of algebra, linear algebra, vectors
and trigonometry.

These modules live at `geo_utilities.optimized_math`, and are written in Cython.

### Calculating what is the closest way (street) to a coordinate
`geo_utilities.optimized_math.cartesian`

To calculate what is the closest way to a coordinate, first we should assume the following factors:

* Ways are line segments created from the main way split by its altitude intersections, and will be referred to as line
segments.
* The coordinate position, in WGS84, is a point. 
* The nature of the point is inaccurate, as the reported GPS position could and
will be outside the line segment, as these segments have no area.
* Calculating based on a circular system is expensive, and slow, due to geometric caveats.

We need to find the shortest distance from the point, to the line segment, this operation is known as an
orthogonal projection of a point onto a line segment.

To get us started, we will convert the WGS84 coordinates onto its 3D cartesian equivalent. Doing this, we will lose a 
fair amount of accuracy, but gain speed and ease of calculation. As the point should be fairly close to the line
segments, *50m*¹ or closer, accuracy is not too important here.

To do so, consider the following formula, where:

* a = Latitude
* b = Longitude
* R = Earth Radius, approx 6378.1370
* x, y, z = Cartesian Coordinates
* super c = Radians symbol, for the uninitiated

![equation](https://i.ibb.co/8M4TL9d/image.png)

![equation](https://i.ibb.co/RPzG3yN/image.png)

![equation](https://i.ibb.co/3yrTg72/image.png)

Repeat for point, and line segment (line segment is made out of two points). Now we have two components, in cartesian
coordinates.

Then, convert the line segment to a vector, where:

* v = Resulting vector
* x = Position

![equation](https://i.ibb.co/2hxLkMq/image.png)

Repeat for each dimension, using the line segment points as x, y and z.

![equation](https://i.ibb.co/3rdC4hH/image.png)

Do the same with the point, where x1 = Point, and x2 = Line segment x1, the result will be the vector vpx.

The result will be vlx, vly, vlz for the line segment, and vpx, vpy, vpz for the point.

Now we are going to scale the vectors by the length of vl, and calculate the dot product. We start by calculating the
length we are going to scale with, using Pythagoras Theorem:

![equation](https://i.ibb.co/WP6KwmB/image.png)

The scale factor afterwards:

![equation](https://i.ibb.co/r7gLJLX/image.png)

Scale, and calculate dot product, resulting in the distance of vlx perpendicular to vpx as t:

![equation](https://i.ibb.co/4Zxptqw/image.png)

Clamp t:

![equation](https://i.ibb.co/5kf3p5m/image.png)

Scale it back, and calculate the distance between the clamped dot, to the point, where d is the resulting distance.

![equation](https://i.ibb.co/m45bshh/image.png)

¹Varies on Nominatim, and Overpass configuration.

### Calculating long distances accurately
`geo_utilities.optimized_math.haversine`

Uses the Haversine formula (https://en.wikipedia.org/wiki/Haversine_formula) implementation in C for these calculations.