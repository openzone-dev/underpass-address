# Verbose Address
`geo_utilities.verbose.VerboseAddress`

VerboseAddress is a customizable class, its purpose is to translate address data, to a human-readable forms, that are 
then output as a result. OSM nodes usually follow a common structure, so the verbosity of Nominatim and Overpass
results is virtually the same. 

Nonetheless, some countries have specific quirks to their structures, and these can be introduced to the class. 

By default, the language is set to Spanish, and the quirks are set to Colombia, Latin America. These values can easily
be changed to accommodate any other language, and country. The support for multiple languages is not yet planned, but
it could be introduced.

On the other hand, if multiple addresses from multiple countries have to be decoded and verbose, refer to the main
README.md for recommendations.

## Modes

Depending on the position of the coordinates, one of three modes will be
triggered. These conditions are found at `geo_utilities.verbose.VerboseAddress.is_countryside`

1. City
2. Countryside
3. History

The verbosity of these parts is handled by `geo_utilities.verbose.VerboseAddress`

### City

The city mode is triggered if the coordinates are detected inside a *city node*¹, and some other
conditions are met. If on effect, the address will be calculated with *altitude*², like so:

`Calle 48 entre Carrera 73 Bis y Carrera 73, Normandía Occidental, Localidad Engativá, Bogotá, Colombia`

Where "Calle 48" is the main street the coordinates are closer to, and "Carrera 73 Bis" with 
"Carrera 73" are the altitude streets to the main street, those being
the next altitude available, and the one before.

When only one altitude is available, or none, the output will change to reflect this.

The rest of the address is composed of `["neighbourhood", "suburb", "city", "region", "country"]`, in that order,
when available.

It will use the Cartesian formula from the `geo_utilities.optimized_math.cartesian` module, to know which part (segment) of
the street is closer to the input coordinates. These segments are generated from splitting the main street into parts,
where each altitude cuts the street into a segment. The data passed to the math module would look like this:

![example](https://i.ibb.co/KFHg4Ld/Screenshot-from-2021-11-12-12-16-18.png)

All the segments are iterated through, and the closest one is considered to be the segment where the coordinates are 
located.

¹City Node: A geometry object, its children are the city contents. All the logic is done by Nominatim, and is 
extremely fast.

²Altitude: Refers to places where streets intersect, ej: right turns.

### Countryside

This mode is activated when the address is not inside a city, or is inside a town.
As with the other modes, some more conditions are involved for fine-tuning.

When on effect, addresses will be returned including the distance to the closest *landmark*¹, like this:

`Vía Mediacanoa - Loboguerrero a 11.8 km de Dagua, Dagua, Valle del Cauca, Colombia`

Where "Vía Mediacanoa - Loboguerrero" is the current way, like a highway or country-road, and "Dagua" is
the closest landmark, 11.8 km away.

If no current way name is available, that part will be omitted, and the distance will be reported as first
part of the address.

The rest of the address is composed of `["town", "hamlet", "county", "state", "region", "country"]`, in that order,
when available.
 
The distance is calculated using the haversine formula from the `geo_utilities.optimized_math.haversine` module. To
avoid querying to the database again, causing overhead, a local file with all the relevant landmarks is kept in the
filesystem as a Pickle file. Its location can be set with an environment variable. This Pickle file will be loaded from
disk to RAM when the microservice is run, and if it does not exist, it will be created from a query to the database.

Access to this Pickle file and creation is handled by `geo_utilities.landmark_distance.LandmarkDistance`. Note that this
class should only be loaded once to memory, and is thread-safe. Further references should be handled statically.

¹Landmark: A geo node of importance, it currently contains cities and towns.

### History

This mode is a WIP alternative to countryside, it should use the history of the vehicle (last position) 
to calculate what's the closest *landmark*¹ the vehicle is heading to. Whereas other modes work with
any coordinates, this mode should only operate with moving objects.

¹Landmark: A geo node of importance, it currently contains cities and towns.