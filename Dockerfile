# Builder image, it creates the wheels to install to the final images, and compiles
# relevant files.
FROM python:3.10.0-bullseye AS builder

WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

# Install build tools for Cython
RUN apt-get update \
&& apt-get -y install build-essential

# Compile Python dependencies to wheels
COPY requirements.txt .
RUN pip3 install --no-cache-dir --upgrade pip
RUN pip3 wheel --no-cache-dir --no-deps --wheel-dir /usr/src/wheels -r requirements.txt
RUN pip3 install --no-cache /usr/src/wheels/*

# Compile Cython Math
COPY src/geo_utilities/optimized_math .
RUN python3 setup.py build_ext --inplace

# Main image, it has the shared layers between development and production.
FROM python:3.10.0-bullseye AS main

# Universal buffer workaround (refer to Python base image for details)
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

# Flask env
ENV FLASK_APP=geo_reverser
ENV PYTHONPATH=/home/app/web/

# Install netcat to run the entrypoint.sh
RUN apt-get update \
&& apt-get -y install netcat

# Create app directories
ENV HOME=/home/app
ENV APP_HOME=/home/app/web
RUN mkdir -p $APP_HOME && mkdir -p $HOME

# Install the wheels compiled in the builder image
COPY --from=builder /usr/src/wheels /wheels
RUN pip3 install --no-cache-dir --upgrade pip
RUN pip3 install --no-cache /wheels/*
RUN rm -rf /wheels

# Development image, it uses entrypoint.dev.sh to compile the math module everytime the service
# starts.
FROM main AS development

ENV FLASK_ENV=development

# Install build tools for Cython
RUN apt-get update \
&& apt-get install -y build-essential

# Copy entrypoint
COPY entrypoints/entrypoint.dev.sh /home/app/entrypoint.sh
RUN sed -i 's/\r$//g'  /home/app/entrypoint.sh
RUN chmod +x  /home/app/entrypoint.sh

WORKDIR $APP_HOME
CMD ["/home/app/entrypoint.sh"]

# Production image, it depends on the already built math modules from the builder, and includes the files.
FROM main AS production

# Copy entrypoint
COPY entrypoints/entrypoint.prod.sh /home/app/entrypoint.sh
RUN sed -i 's/\r$//g'  /home/app/entrypoint.sh
RUN chmod +x  /home/app/entrypoint.sh

# Create tmp and storage folders
RUN mkdir -p /var/lib/geo-address && mkdir -p /var/tmp/geo_cache

# Copy app
WORKDIR $APP_HOME
COPY src/ .

# Copy built modules
COPY --from=builder /usr/src/app/*.so geo_utilities/optimized_math/

CMD ["/home/app/entrypoint.sh"]