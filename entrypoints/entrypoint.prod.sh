#!/bin/sh

echo "Waiting for Nominatim..."

while ! nc -z "$NOMINATIM_HOST" "8080"; do
    sleep 5
done

echo "Waiting for Overpass-API..."

while ! nc -z "$OVERPASS_HOST" "80"; do
    sleep 5
done

echo "Everything done, starting app..."

cd /home/app/web/ && gunicorn --workers="$WORKERS" --threads="$THREADS_PER_WORKER" --bind 0.0.0.0:"$PORT" wsgi:app
# exec "$@"