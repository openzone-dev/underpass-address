#!/bin/sh

echo "Waiting for Nominatim..."

while ! nc -z "$NOMINATIM_HOST" "8080"; do
    sleep 5
done

echo "Waiting for Overpass-API..."

while ! nc -z "$OVERPASS_HOST" "80"; do
    sleep 5
done

echo "Compiling Math..."

cd /home/app/web/geo_utilities/optimized_math/ && python setup.py build_ext --inplace

echo "Everything done, starting app..."

cd /home/app/web/ && python3 -m flask run --host=0.0.0.0 --port="$PORT"
# exec "$@"